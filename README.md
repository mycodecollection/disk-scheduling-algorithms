# Disk Scheduling Algorithms
## Description
Disk scheduling is the process of organizing and managing access to disks on a computer system. The goal of disk scheduling is to optimize disk usage by reducing access time and improving overall system performance.

Below is a brief description of some types of disk scheduling algorithms that are commonly used:
- FIFO (First-In-First-Out): In FIFO, disk access is organized based on the order of requests that arrive first. Requests that arrive earlier will be served first.
- SSTF (Shortest Seek Time First): SSTF prioritizes disk access based on the shortest seek time range. Requests that have the shortest seek time range to the current position will be served first.
- SCAN: SCAN moves unidirectionally on the disk from one end to the other in sequence, serving requests along the path. Upon reaching the end, the disk head will reverse direction and continue traveling in the reverse direction. Requests along the path will be served.
- CSCAN (Circular SCAN): CSCAN is a variation of the SCAN method. After reaching the end of the disk, the disk head returns to the initial end without reversing direction. In this method, requests along the path will be serviced, and after reaching the end, the disk head will return to the beginning and process pending requests along the path.

## References
- [Disk Scheduling Algorithms](https://www.geeksforgeeks.org/disk-scheduling-algorithms)
- [Disk Scheduling Tutorial](https://www.youtube.com/watch?v=yrO5fvXlESE)
- [Interactive Example](https://www.cs.usask.ca/faculty/makaroff/cgi-bin/disk_sched.pl)
